package com.cloud.admin.BootAdminServer;

import com.cloud.admin.configurations.AdminSecurityConfiguration;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableAdminServer
@Import(AdminSecurityConfiguration.class)
public class BootAdminServerApplication {



	public static void main(String[] args) {
		SpringApplication.run(BootAdminServerApplication.class, args);
	}

}
